# Récupération des données depuis hdfs 

## Utilisation

Se scritp dois être exécuté sur un des Datanode du cluster avec un user ayant les droits d'accès HDFS
### Parametration

Les variables à modifier sont dans le fichier **env.config**

La variable *hdfs_root* chemin d'accès des données sur hdfs
La variable *fs_root* correspond au dossier de dépose des fichiers.

### Lancement 

Utilisez la commande
```
./get_hdfs_data.sh
```
